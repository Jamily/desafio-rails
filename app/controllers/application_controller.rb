class ApplicationController < ActionController::Base
  before_action :authenticate_user!, only: [ :create ]

  layout 'admin_lte_2'

end
