class Book < ApplicationRecord
  belongs_to :author
  belongs_to :category

  def self.search(query)
    where("name like ?", "%#{query}%")
  end
end
