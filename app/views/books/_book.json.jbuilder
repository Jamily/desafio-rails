json.extract! book, :id, :title, :author_id, :category_id, :year, :of, :publication, :created_at, :updated_at
json.url book_url(book, format: :json)
