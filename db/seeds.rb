# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Gerando as categorias dos livros..."
    10.times do |i|
    Category.create!(
        description: Faker::Book.genre,
    )
    end
puts "Gerando as categorias dos livros...[OK]"

puts "Gerando os bibliotecários..."
    10.times do |i|
    Librarian.create!(
        name: Faker::Name.name,
        email: Faker::Internet.email
    )
    end
puts "Gerando os bibliotecários...[OK]"

puts "Gerando os autores..."
    10.times do |i|
    Author.create!(
        name: Faker::Book.author,
        category: Category.all.sample
    )
    end
puts "Gerando os autores...[OK]"

puts "Gerando os livros..."
    10.times do |i|
    Book.create!(
        title: Faker::Book.title,
        stock: Faker::Number.number(digits: 2),
        author: Author.all.sample,
        category: Category.all.sample,
        year: Faker::Number.between(from: 1300, to: 2019)
    )
    end
puts "Gerando os livros...[OK]"

puts "Gerando as reservas..."
    10.times do |i|
    Reservation.create!(
        client: Client.all.sample,
        book: Book.all.sample,
        librarian: Librarian.all.sample
    )
    end
puts "Gerando as reservas...[OK]"

puts "Gerando os clientes..."
    10.times do |i|
    Client.create!(
        name: Faker::Name.name_with_middle,
        telephone: Faker::PhoneNumber.cell_phone,
        CPF: Faker::Number.number(digits: 11)
    )
    end
puts "Gerando os clientes...[OK]"



