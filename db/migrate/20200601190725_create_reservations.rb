class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.string :client
      t.references :book, foreign_key: true
      t.references :librarian, foreign_key: true

      t.timestamps null: false
    end
  end
end
