class CreateAdminLte2Plugins < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_lte2_plugins do |t|
      t.string :PLUGIN_NAME

      t.timestamps
    end
  end
end
