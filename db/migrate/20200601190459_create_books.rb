class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :title
      t.string :stock
      t.references :author, foreign_key: true
      t.references :category, foreign_key: true
      t.string :year

      t.timestamps null: false
    end
  end
end
