Rails.application.routes.draw do
  devise_for :users
  resources :clients
  get 'home/index' #controller/acction
  root 'home#index'
  
  resources :reservations #resources cria as 8 rotas de forma automática
  resources :books
  resources :authors
  resources :librarians
  resources :categories
  resources :clients

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #Outra forma de criar rotas: 
  #only:[:new], show] (mostra somente esses)
  #except: :edit (exclui)
end
